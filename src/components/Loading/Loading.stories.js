import SlpLoadingLinear from "./LoadingLinear";
import SlpLoadingRound from "./LoadingRound";

export default {
  title: "Components/Loading",
  argTypes: {},
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const TemplateLoadingLinear = (args, { argTypes }) => ({
  components: { SlpLoadingLinear },
  props: Object.keys(argTypes),
  template: '<SlpLoadingLinear v-bind="$props"></SlpLoadingLinear>',
});

const TemplateLoadingRound = (args, { argTypes }) => ({
  components: { SlpLoadingRound },
  props: Object.keys(argTypes),
  template: '<SlpLoadingRound v-bind="$props"></SlpLoadingRound>',
});

export const LoadingLinear = TemplateLoadingLinear.bind({});
export const LoadingRound = TemplateLoadingRound.bind({});
