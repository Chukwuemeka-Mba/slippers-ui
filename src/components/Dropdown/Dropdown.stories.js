import SlpDropdown from "./Dropdown.vue";
import SlpDropdownTest from "./DropdownTest.vue";
import SlpCustomDropdown from "./CustomDropdown.vue";
import SlpCustomDropdownTest from "./CustomDropdownTest.vue";

export default {
  title: "Components/Dropdown",
  component: SlpDropdown,
  SlpDropdownTest,
  SlpCustomDropdown,
  SlpCustomDropdownTest,
  argTypes: {},
};

const Template = (args, { argTypes }) => ({
  components: { SlpDropdown },
  props: Object.keys(argTypes),
  template: `<SlpDropdown v-bind="$props"></SlpDropdown>`,
});

const PressureTest = (args, { argTypes }) => ({
  components: { SlpDropdownTest },
  props: Object.keys(argTypes),
  template: `<SlpDropdownTest v-bind="$props"></SlpDropdownTest>`,
});

const CustomTemplate = (args, { argTypes }) => ({
  components: { SlpCustomDropdown },
  props: Object.keys(argTypes),
  template: `<SlpCustomDropdown v-bind="$props"></SlpCustomDropdown>`,
});

const CustomDropdownTest = (args, { argTypes }) => ({
  components: { SlpCustomDropdownTest },
  props: Object.keys(argTypes),
  template: `<SlpCustomDropdownTest v-bind="$props"></SlpCustomDropdownTest>`,
});

export const NoProps = Template.bind({});
NoProps.args = {
  name: "no-props",
};

export const WithProps = Template.bind({});
WithProps.args = {
  name: "with-props",
  options: ["Sales", "Engineering", "Marketing", "Legal"],
  value: "Marketing",
};

export const Test = PressureTest.bind({});

export const CustomTest = CustomDropdownTest.bind({});

export const Custom = CustomTemplate.bind({});
Custom.args = {
  name: "with-props",
  options: ["Sales", "Engineering", "Marketing", "Legal"],
  value: "Marketing",
};
