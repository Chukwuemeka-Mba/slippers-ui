import SlpCard from "./Card.vue";
import SlpContainer from "../Container/Container.vue";
import SlpColumn from "../Column/Column.vue";
import SlpLink from "../Link/Link.vue";
import SlpRow from "../Row/Row.vue";
import SlpTypography from "../Typography/Typography.vue";

export default {
  title: "Components/Card",
  component: SlpCard,
  argTypes: {
    variantType: {
      control: { type: "select" },
      options: ["primary", "secondary"],
    },
    variantNumber: {
      control: { type: "select" },
      options: [1, 2, 3, 4],
    },
  },
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: {
    SlpContainer,
    SlpRow,
    SlpColumn,
    SlpCard,
    SlpLink,
    SlpTypography,
  },
  props: Object.keys(argTypes),
  template: `
    <SlpContainer>
      <SlpRow>
        <SlpColumn :cols="4">
          <SlpCard v-bind="$props" class="slp-storybook-card-example">
            <div>
              <p class="slp-mb-8">
                <SlpTypography variant="all-caps">
                  Partners
                </SlpTypography>
              </p>
              <p class="slp-mb-32">
                <SlpTypography variant="heading5">
                  Discover the benefits of...
                </SlpTypography>
              </p>
            </div>
            <SlpLink href="https://about.gitlab.com" :arrow="true">
              Learn more
            </SlpLink>
          </SlpCard>
        </SlpColumn>
      </SlpRow>
    </SlpContainer>
  `,
});

export const Default = Template.bind({});
Default.args = {
  variantType: "primary",
  variantNumber: 3,
};
