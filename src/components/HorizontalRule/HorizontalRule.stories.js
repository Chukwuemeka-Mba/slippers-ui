import SlpHorizontalRule from "./HorizontalRule.vue";

export default {
  title: "Components/HorizontalRule",
  component: SlpHorizontalRule,
  argTypes: {},
};

// More on component templates: https://storybook.js.org/docs/vue/writing-stories/introduction#using-args
const Template = (args, { argTypes }) => ({
  components: { SlpHorizontalRule },
  props: Object.keys(argTypes),
  template: `
    <div>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<p>
      <SlpHorizontalRule class="slp-my-32" v-bind="$props" />
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer auctor ipsum vel euismod commodo. Ut ullamcorper pellentesque lacus quis malesuada. Nulla quis posuere tortor. Curabitur vel commodo orci. In diam orci, interdum et enim sit amet, finibus sodales dolor. Quisque luctus neque sit amet nunc pellentesque commodo sed et tortor. Nunc porttitor rutrum dui, eget lacinia nulla.
      Donec volutpat in purus sed sagittis.</p>
    </div>
  `,
});

export const Default = Template.bind({});
Default.args = {};
