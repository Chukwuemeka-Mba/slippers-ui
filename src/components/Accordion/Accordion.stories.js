import Accordion from "../Accordion/Accordion.vue";

export default {
  title: "Components/Accordion",
  component: Accordion,
  argTypes: {
    variant: {
      control: { type: "text" },
    },
  },
};

const Template = (args, { argTypes }) => {
  return {
    components: { Accordion },
    props: Object.keys(argTypes),
    template: `
          <Accordion v-bind="$props"/>
      `,
  };
};

export const Default = Template.bind({});

Default.args = {
  heading: "Lorem ipsum dolor sit amet",
  accordions: [
    {
      isOpen: false,
      title: "Lorem ipsum dolor sit amet",
      subtitle: "Feugiat morbi eleifend at sed orci?",
      desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius id in fringilla eget vel consectetur quis sagittis pretium. Sed eu cum est, egestas scelerisque.",
    },
    {
      isOpen: false,
      title: "Lorem ipsum dolor sit amet",
      subtitle: "Feugiat morbi eleifend at sed orci?",
      desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Varius id in fringilla eget vel consectetur quis sagittis pretium. Sed eu cum est, egestas scelerisque.",
    },
  ],
};
