import Icon from "./Icon";

const requireIcons = require.context("../../static/icons/", false, /.svg$/);

const availableIcons = requireIcons
  .keys()
  .map((fileName) => `slp-${fileName.replace(/^\.\/(.+)\.svg$/, "$1")}`);

export default {
  title: "Components/Icon",
  component: Icon,
  argTypes: {
    icon: {
      control: { type: "select" },
      options: [
        "gl-archive",
        "gl-attention-solid",
        "gl-clear",
        "gl-heart",
        "gl-incognito",
        ...availableIcons,
      ],
    },
    size: {
      control: { type: "select" },
      options: ["xs", "sm", "md", `lg`, "xl"],
    },
    color: {
      control: { type: "select" },
      options: [
        "black",
        "surface-300",
        "surface-950",
        "#000000",
        "#aaf",
        "#f04",
      ],
    },
  },
};

const TemplateNew = (args, { argTypes }) => ({
  components: { Icon },
  props: Object.keys(argTypes),
  template:
    '<Icon style="padding:50px 0 0 200px" v-bind="$props" >{{icon}}</Icon>',
});

export const Default = TemplateNew.bind({});
Default.args = {
  icon: "slp-crown",
  size: "md",
  color: "surface-300",
};

const Template = (args, { argTypes }) => ({
  components: { Icon },
  props: Object.keys(argTypes),
  template: `
    <div style="padding: 50px; display: flex; flex-direction: column">
      <Icon name="archive" variant="product" size="lg" hexColor="#4f0"/>
      <Icon name="article" variant="marketing" size="lg" hexColor="#f00"/>
      <Icon name="archive" variant="product" size="lg" slpColor="success-100"/>
      <Icon name="bullet" variant="marketing" size="lg" slpColor="accent-500"/>
    </div>`,
});

export const Deprecated = Template.bind({});
Deprecated.args = {
  name: "archive",
  variant: "product",
  slpColor: "surface-300",
};
