export const throttle = (func, wait = 100) => {
  let timer = null;
  return function (...args) {
    if (timer === null) {
      timer = setTimeout(() => {
        func.apply(this, args);
        timer = null;
      }, wait);
    }
  };
};

/**
 * Stop default event handling and propagation
 */
export function stopEvent(
  event,
  {
    preventDefault = true,
    stopPropagation = true,
    stopImmediatePropagation = false,
  } = {}
) {
  if (preventDefault) {
    event.preventDefault();
  }
  if (stopPropagation) {
    event.stopPropagation();
  }
  if (stopImmediatePropagation) {
    event.stopImmediatePropagation();
  }
}

/**
 *
 * @param {*} number
 * @param {*} min
 * @param {*} max
 * @returns clamp number between min and max
 */
export function clamp(number, min, max) {
  return Math.max(min, Math.min(number, max));
}
