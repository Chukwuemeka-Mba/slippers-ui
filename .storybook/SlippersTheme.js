import { create } from '@storybook/theming/create';

export default create({
  // Typography
  fontBase: '"Inter", sans-serif',

  // Text colors
  base: 'light',
  brandTitle: 'Slippers Design System',
  brandUrl:
    'https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui',
  brandImage: 'slippers_logo.svg',
});
