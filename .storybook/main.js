const path = require('path');

module.exports = {
  stories: [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  addons: [
    '@storybook/addon-docs',
    {
      name: "@storybook/addon-essentials",
      options: {
        actions: false,
        docs: false,
      },
    },
  ],
  staticDirs: [
    '../src/static'
  ],
  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Remove 'svg' from the existing regex covering all images
    const svgRule = config.module.rules.find(rule => typeof rule.test.test === 'function' && rule.test.test('.svg'));
    svgRule.test = new RegExp(svgRule.test.source.replace('svg|', ''))

    // Make whatever fine-grained changes you need
    config.module.rules.push({
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
      include: path.resolve(__dirname, '../'),
    });

    config => config.plugin.delete('prefetch')

    config.module.rules.push({
      test: /\.svg$/,
      use: [
        {
          loader: 'html-loader',
          options: {
            minimize: true
          }
        }
      ]
    });
    return config;
  },
  framework: "@storybook/vue"
}
