## Goal

We would like to keep our Slippers Figma library up-to-date which will improve consistency across design and development as measured by more reproducible work and less one off's.

### Jobs To Be Done

- **Situation**: _When anyone has an update (add, edit, or remove) for something in the Slippers Figma file..._
- **Motivation**: _we want to have one designer designated DRI per sprint overseeing the work..._
- **Outcome**: _so we can keep track of updates and efficiently answer questions as they arise._

## Page(s)

Which page(s) are involved in this request?

- [Slippers Figma](https://www.figma.com/file/nWIOpmuMp7RZXmfTj6ujAF/Slippers_foundations?node-id=2110%3A1418)

## Released

Which element, component, and/or block has changed?

1.
1.
1.

The [Slippers design documentation](https://docs.google.com/document/d/1AHYoKi2rPT4R31faQdV4TnRam74sNbfDEDdFpq7_M_Y/edit) I updated this sprint is:

1.

## DCI

[DRI, Consulted, Informed](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#dri-consulted-informed-dci)

- [ ] DRI: `@Tinaliseng` `@jhalloran`
- [ ] Consulted: `@Tinaliseng` `@jhalloran`
- [ ] Informed: `Everyone`

## In scope

What is within scope of this request?

- Any update to the Figma Slippers file

## Out of scope

What is out of scope and not part of this iteration?

- TBD

## Requirements

What are the requirements for this request? Checklist below is an example of common requirements, please check all that apply and adjust as necessary:

- [ ] Copy writing
- [ ] Illustration
- [ ] Custom Graphics
- [ ] Research
- [ ] Data / Analytics
- [x] UX Design
- [ ] Engineering
